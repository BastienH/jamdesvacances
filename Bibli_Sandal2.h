#ifndef __BIBLI_SANDAL2_H__
#define __BIBLI_SANDAL2_H__

#include "structures.h"
#include "musicV2.h"
#include "mouvement.h"
#include "niveau.h"
#include "interface.h"

/* Fonctions */

/***********************************
 * Nom :
 *
 * Objectif :
 *
 *
 *
 *
 * Paramètres :
 *  -
 *  -
 *  -
 *  -
 *  -
 *
 * Retour :
 *
 *
 *
***********************************/
//placer la fonction ici ( ! ne pas oublier de rajouter le nom de la fonction dans le ReadMe ! )



/***********************************
 * Nom : initGame
 *
 * Objectif : initialise Sandal2, SDL_mixer ainsi qu'une nouvelle fenêtre
 *
 * Paramètres :
 *  - longueur : un entier, la longueur de la fenêtre
 *  - largeur : un entier, la largeur de la fenêtre
 *  - nom : string, nom de la fenêtre
 *  - couleur : un tableau contenant la couleur du fond de la fenêtre
 *  - boolMusic : un entier qui vaut 0 si on ne veut pas de musique, un autre nombre sinon
 *
 * Retour : 0 si réussite, -1 ou -2 si échec
 *
***********************************/

int initGame(int longueur, int largeur, char * nom, int couleur[4], int boolMusic);

/***********************************
 * Nom : runGame
 *
 * Objectif : fait tourner la Sandal2
 *
 * Paramètres :
 *  - i : un entier, temps de reset des balayages d'écran
 *
 * Retour :
 *  un entier valant zéro tant que le jeu n'est pas fermé, un autre entier sinon
 *
***********************************/

int runGame(int i);

/***********************************
 * Nom : endGame
 *
 * Objectif : ferme toutes les fenêtres ainsi que Sandal2
 *
 * Paramètres : aucun
 *
 * Retour : aucun
 *
***********************************/

void endGame(void);

/*
Action * initAction(int nbAction, char ** tabAction, char * tabCommande)
{
  Action * ac = malloc(sizeof(Action));

  if(ac)
  {
    ac->nbAction = nbAction;
    ac->tabAction = tabAction;
    ac->tabCommande = tabCommande;
  }
  return ac;
}

void Show(Action * ac)
{
  if(ac)
  {
    printf("nb:%d\n", ac->nbAction);
    for(int i = 0; i < ac->nbAction; i++)
    {
      printf("A:%s C:%d", ac->tabAction[i],ac->tabCommande[i]);
    }
  }
  else
  {
    puts("ac non allouée");
  }
}

void libereAction(Action * ac)
{
  if(ac)
  {
    free(ac);
  }
}

Action * initAction(int nbAction, char ** tabAction, char * tabCommande);
void Show(Action * ac);
void libereAction(Action * ac);
*/

void initButton(int x, int y, int taille, char * txt, int colorTxt[4], int colorClique[4], int colorBloc[4], char * source, int option, int param, int display, int plan);

void CliqueBtn(Element * this, int i);

void UnCliqueBtn(Element * this, int i);

#endif
