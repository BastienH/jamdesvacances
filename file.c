#include "file.h"

/*----------------------------------------------------------------------*/
/*inti_file      Initialise une file de taille n						*/
/*																		*/
/*En entrée : n un entier représentant la taille de la file créée		*/
/*																		*/
/*En sortie : f le pointeur de la file créée							*/
/*																		*/
/*Principe : allocation de la struture contenant les info de la file	*/
/*				à l'adresse f											*/
/*			 Si allocation réussi alors									*/
/*			 | allocation de la file à l'adresse f->file				*/
/*			 Fait														*/
/*Lexique : n taille de la file*/
/*					f pointeur de tete*/
/*----------------------------------------------------------------------*/

file_t * init_file (int n)
{
	file_t * f;

	f = (file_t*)malloc(sizeof(file_t));

	if (f){

		for(int i=0;i<n;i++){
			f->file=NULL;
		}
		f->taille=n;
		f->NbElem=-1;

	}


	return f;
}

/*----------------------------------------------------------------------*/
/*file_vide     renvoie 1 si la file est vide 0 sinon						*/
/*																		*/
/*En entrée : f pointeur de  tete	*/
/*																		*/
/*En sortie : un booleen							*/
/*																		*/
/*Principe : File vide si NbElem=-1		*/
/*								*/
/*Lexique :  f pointeur de tete*/
/*----------------------------------------------------------------------*/

int file_vide (file_t * f)
{
	return (f->NbElem==-1);
}


/*----------------------------------------------------------------------*/
/*file_pleine     renvoie 1 si la file est pleine 0 sinon						*/
/*																		*/
/*En entrée : f pointeur de  tete	*/
/*																		*/
/*En sortie : un booleen							*/
/*																		*/
/*Principe : File pleine si rang du dernier = NbElem		*/
/*								*/
/*Lexique :  f pointeur de tete*/
/*----------------------------------------------------------------------*/

int file_pleine (file_t * f)
{
	return (f->NbElem==f->taille-1);
}

/*----------------------------------------------------------------------*/
/*entree      ajoute une valur en fin de file						*/
/*																		*/
/*En entrée : f pointeur de tete*/
/*						v la valeur directe à ajouter		*/
/*																		*/
/*En sortie : un booleen 0 si echec, 1 si réussite							*/
/*																		*/
/*Principe : Si la file n'est pas pleine*/
/*					 |	incrémenter le NbElem*/
/*					 |	ajouter la valeur en fin*/
/*					 |	modifier le rg de fin	*/
/*					 Fsi*/
/*					 retourner le booleen									*/
/*																													*/
/*Lexique : v valeur directe à ajouter*/
/*					f pointeur de tete*/
/*----------------------------------------------------------------------*/

void entree (file_t * f, ElemFile v)
{
	cell_t * newcell = malloc(sizeof(cell_t));
	cell_t * cour = f->file, **prec = &(f->file);
	if (!file_pleine(f))
	{
		newcell->mob = v;
		newcell->suivant = NULL;

		while(cour){
			prec = &(cour->suivant);
			cour = cour->suivant;
		}
		*prec = newcell;
		(f->NbElem)++;
	}

}

/*----------------------------------------------------------------------*/
/*sortie      recupère la valeur à traiter					*/
/*																		*/
/*En entrée : f pointeur de tete*/
/*						ok booleen 0 si echec, 1 si réussite		*/
/*																		*/
/*En sortie : la valeur a traiter							*/
/*																		*/
/*Principe : Si la file n'est pas vide*/
/*					 |	décrémenter le NbElem*/
/*					 |	récupérer la valeur à traiter*/
/*					 |	modifier le rg de fin	*/
/*					 Fsi*/
/*					 retourner la valeur									*/
/*																													*/
/*Lexique : res valeur à traiter*/
/*					ok booleen*/
/*					f pointeur de tete*/
/*----------------------------------------------------------------------*/

ElemFile sortie (file_t * f)
{
	ElemFile res;
	cell_t * temp;

	if (!file_vide(f))
	{
		res = f->file->mob;
		temp = f->file;
		f->file = f->file->suivant;;
		free(temp);
		(f->NbElem)--;
	}
	return res;
}

/*----------------------------------------------------------------------*/
/*liberer     libère la file						*/
/*																		*/
/*En entrée : f pointeur de  tete	*/
/*																		*/
/*En sortie : void							*/
/*																		*/
/*Principe :libérer la file		*/
/*								*/
/*Lexique :  f pointeur de tete*/
/*----------------------------------------------------------------------*/

void liberer (file_t * f)
{
	free (f->file);
	free (f);
}


ElemFile sommet (file_t * f)
{
	ElemFile res;

	if (!file_vide(f))
	{
		res=f->file->mob;
	}
	return res;
}

cell_t * rechercheFile(file_t * f, ElemFile v){
	cell_t * cour = f->file, **prec = &(f->file);
	while(cour && cour->mob != v){
		prec = &(cour->suivant);
		cour = cour->suivant;
	}
	return *prec;
}
