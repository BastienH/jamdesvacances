#include "game.h"

void initTerrain(int largeur, int hauteur){

  //int white[4] = {255,255,255,0};

  createImage(0,4.0/5*hauteur,largeur,1.0/5*hauteur,"Asset/sol.png",ECRAN_GAUCHE,0);
  //createBlock(0,2.0/5*hauteur,1.0/10*largeur,2.0/5*hauteur,white,ECRAN_GAUCHE,0);
  Element * fleche_gauche = createImage(19.0/20*largeur,0,1.0/20*largeur,hauteur,"Asset/fleche_gauche.png",ECRAN_GAUCHE,0);

  createImage(0,4.0/5*hauteur,largeur,1.0/5*hauteur,"Asset/sol.png",ECRAN_DROITE,0);
  //createBlock(9.0/10*largeur,2.0/5*hauteur,1.0/10*largeur,2.0/5*hauteur,white,ECRAN_DROITE,0);
  Element * fleche_droite = createImage(0,0,1.0/20*largeur,hauteur,"Asset/fleche_droite.png",ECRAN_DROITE,0);

  addClickableElement(fleche_gauche,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(fleche_gauche,GaucheVersDroite);

  addClickableElement(fleche_droite,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(fleche_droite,DroiteVersGauche);

}

void GaucheVersDroite(Element * fleche, int i){
  (void)fleche;
  (void)i;
  setDisplayCodeWindow(ECRAN_DROITE);
}

void DroiteVersGauche(Element * fleche, int i){
  (void)fleche;
  (void)i;
  setDisplayCodeWindow(ECRAN_GAUCHE);
}

void createMob(int ecran, int type, Element * lchMob, file_t * file){

  Element * mob;
  //int red[4] = {255,0,0,0};
  //int green[4] = {0,255,0,0};
  char sprite_gauche[3][3][50] = {{"Asset/gauche_pre_masse.png", "Asset/gauche_pre_massue.png", "Asset/gauche_pre_lance_pierre.png"}, {"Asset/gauche_medieval_lance.png", "Asset/gauche_medieval_epee.png", "Asset/gauche_medieval_arc.png"}, {"Asset/gauche_moderne_bazooka.png", "Asset/gauche_moderne_pistolet.png", "Asset/gauche_moderne_grenade.png"}};
  char sprite_droite[3][3][50] = {{"Asset/droite_pre_masse.png", "Asset/droite_pre_massue.png", "Asset/droite_pre_lance_pierre.png"}, {"Asset/droite_medieval_lance.png", "Asset/droite_medieval_epee.png", "Asset/droite_medieval_arc.png"}, {"Asset/droite_moderne_bazooka.png", "Asset/droite_moderne_pistolet.png", "Asset/droite_moderne_grenade.png"}};
  int caracteristiques[] = {5, 100, 100, 25, 3, 3, 50, 60, 17, 5, 2, 40, 35, 14, 7};
  mob_t * structMob = malloc(sizeof(mob_t));
  lch_t * interface;

  getDataElement(lchMob, (void **)&interface);

  structMob->ecran = ecran;
  structMob->type = type;
  structMob->sprite = malloc(50*sizeof(char));

  if(ecran == ECRAN_GAUCHE){

    mob = createImage(1.0/10*W_WINDOW,3.0/5*H_WINDOW,1.0/20*W_WINDOW,1.0/5*H_WINDOW,sprite_gauche[interface->epoque_gauche][type],ECRAN_GAUCHE,0);
    structMob->vitesse = caracteristiques[type*5+4];
    strcpy(structMob->sprite, sprite_gauche[interface->epoque_gauche][type]);
    structMob->dmg = caracteristiques[type*5+3]*(interface->epoque_gauche+1);
    majBar(interface->compteur_gauche,1,ECRAN_GAUCHE,0);
    majBar(interface->compteur_droite,1,ECRAN_DROITE,0);

  }else if(ecran == ECRAN_DROITE){

    mob = createImage(17.0/20*W_WINDOW,3.0/5*H_WINDOW,1.0/20*W_WINDOW,1.0/5*H_WINDOW,sprite_droite[interface->epoque_droite][type],ECRAN_DROITE,0);
    structMob->vitesse = -caracteristiques[type*5+4];
    strcpy(structMob->sprite, sprite_droite[interface->epoque_droite][type]);
    structMob->dmg = caracteristiques[type*5+3]*(interface->epoque_droite+1);

  }

  structMob->cout = caracteristiques[type*5];
  structMob->pv_max = caracteristiques[type*5+1];
  structMob->pv = caracteristiques[type*5+1];
  structMob->attack_speed = caracteristiques[type*5+2];
  structMob->timer = caracteristiques[type*5+2];


  entree (file, mob);
  //setActionElement(mob, marche);
  //addElementToElement(lchMob, mob);
  setDataElement(mob,(void *)structMob);

}

void marche(Element * mob, Element * lchMob, file_t ** fileAlliee, file_t ** fileEnnemie, Element * precedent){

  mob_t * structMob, * structMobEnnemie;
  float x, y, w, h;
  float xennemie, yennemie, wennemie, hennemie;
  Element * ennemie;
  lch_t * interface;

  getDataElement(lchMob, (void **)&interface);

  getDataElement(mob, (void **)&structMob);
  getCoordElement(mob, &x, &y);
  getDimensionElement(mob, &w, &h);


  if((structMob->ecran == ECRAN_GAUCHE && x + w + structMob->vitesse > W_WINDOW) || (structMob->ecran == ECRAN_DROITE && x + structMob->vitesse < 0)){

    mob = changement_ecran(mob, *fileAlliee);

  }

  //si c'est le premier
  if(mob == sommet(*fileAlliee)){

    if(!file_vide(*fileEnnemie)){

      ennemie = sommet(*fileEnnemie);
      getDataElement(ennemie, (void **)&structMobEnnemie);

      getCoordElement(ennemie, &xennemie, &yennemie);
      getDimensionElement(ennemie, &wennemie, &hennemie);

      if(structMob->ecran == structMobEnnemie->ecran && ((structMob->vitesse > 0 && x + w + 2*structMob->vitesse >= xennemie) || (structMob->vitesse < 0 && x + 2*structMob->vitesse <= xennemie + wennemie))){

        fight(mob, ennemie, lchMob);

      }else{

        moveElement(mob,structMob->vitesse,0);

      }

    }else{

      moveElement(mob,structMob->vitesse,0);

    }

  //sinon

  }else{

  //printf("%d\n",fileAlliee->NbElem );

    if(precedent){

      getCoordElement(precedent, &xennemie, &yennemie);
      getDimensionElement(precedent, &wennemie, &hennemie);
      getDataElement(precedent, (void **)&structMobEnnemie);

      if(!(structMob->ecran == structMobEnnemie->ecran && ((structMob->vitesse > 0 && x + w + 2*structMob->vitesse >= xennemie) || (structMob->vitesse < 0 && x + 2*structMob->vitesse <= xennemie + wennemie)))) {

        moveElement(mob,structMob->vitesse,0);

      }
    }

  }

  if((structMob->ecran == ECRAN_DROITE && x + w > W_WINDOW) || (structMob->ecran == ECRAN_GAUCHE && x < 0)){

    if(structMob->ecran == ECRAN_DROITE){
      interface->pv_droite = interface->pv_droite - 1;
      majBar(interface->pdv_droite,-1,ECRAN_DROITE,0);
      majBar(interface->compteur_gauche,-1,ECRAN_GAUCHE,0);
      majBar(interface->compteur_droite,-1,ECRAN_DROITE,0);
    }else{
      interface->pv_gauche = interface->pv_gauche - 1;
      majBar(interface->pdv_gauche,-1,ECRAN_GAUCHE,0);
    }
    //printf("%d\n", (*fileAlliee)->NbElem);
    sortie(*fileAlliee);
    //delElementToElement(lchMob, mob);
    delElement(mob);
  }


}

Element * changement_ecran(Element * mob, file_t * f){

  mob_t * structMob;
  Element * newMob;
  float w, h, x, y;
  //int red[4] = {255,0,0,0};
  //int green[4] = {0,255,0,0};

  getDataElement(mob,(void **)&structMob);
  getDimensionElement(mob, &w, &h);
  getCoordElement(mob, &x, &y);


  if(structMob->ecran == ECRAN_GAUCHE){

    newMob = createImage(0, y, w, h, structMob->sprite, ECRAN_DROITE, 0);
    structMob->ecran = ECRAN_DROITE;

  }else{

    newMob = createImage(W_WINDOW - w, y, w, h, structMob->sprite, ECRAN_GAUCHE, 0);
    structMob->ecran = ECRAN_GAUCHE;

  }

  //setActionElement(newMob,marche);
  setDataElement(newMob, (void *)structMob);
  rechercheFile(f,mob)->mob = newMob;
  //delElementToElement(lchMob, mob);
  delElement(mob);
  //addElementToElement(lchMob, newMob);

  return newMob;

}

void fight(Element * mob, Element * ennemie, Element * lchMob){

  mob_t * structMob, * structMobEnnemie;

  getDataElement(mob, (void **)&structMob);

  if(structMob->timer == 0){

    getDataElement(ennemie, (void **)&structMobEnnemie);
    structMob->timer = structMob->attack_speed;
    structMobEnnemie->pv = structMobEnnemie->pv - structMob->dmg;
    //printf("ouch\n");
    if(structMobEnnemie->pv <= 0){
      tmort(ennemie, lchMob);
    }

  }else{

    structMob->timer--;

  }

}

void tmort(Element * mob, Element * lchMob){

  mob_t * structMob;
  lch_t * structLch;

  getDataElement(mob, (void **)&structMob);
  getDataElement(lchMob, (void **)&structLch);



  if(structMob->vitesse > 0){
    sortie(structLch->fileGauche);
    structLch->xp_ennemie = structLch->xp_ennemie + 5;
    majBar(structLch->compteur_gauche,-1,ECRAN_GAUCHE,0);
    majBar(structLch->compteur_droite,-1,ECRAN_DROITE,0);
  }else{
    sortie(structLch->fileDroite);
    structLch->argent = structLch->argent + 10;
    majBar(structLch->argent_gauche,10,ECRAN_GAUCHE,0);
    majBar(structLch->argent_droite,10,ECRAN_DROITE,0);
  }

  //delElementToElement(lchMob, mob);
  delElement(mob);

}

Element * init_interface(){

  int white[4] = {255,255,255,0};
  int largeur_bouton = 100;
  lch_t * interface = malloc(sizeof(lch_t));
  Element * blocInterface = createBlock(0,0,0,0,white,ECRAN_GAUCHE,0);
  bouton_t * bouton_unite = malloc(sizeof(bouton_t));

  bouton_unite->cd = 300;
  bouton_unite->timer = 0;

  interface->xp_ennemie = 0;

  interface->pv_gauche = 10;
  interface->pv_droite = 10;
  interface->pdv_gauche = initBar(1.0/20*W_WINDOW+10, 4.0/5*H_WINDOW + 20, 300, largeur_bouton, interface->pv_gauche, interface->pv_gauche, "Vie : ", ECRAN_GAUCHE, 0);
  interface->pdv_droite = initBar(W_WINDOW - 1.0/20*W_WINDOW-310, 4.0/5*H_WINDOW + 20, 300, largeur_bouton, interface->pv_droite, interface->pv_droite, "Vie : ", ECRAN_DROITE, 0);

  interface->compteur_gauche = initBar(1.0/20*W_WINDOW+320, 4.0/5*H_WINDOW + 20, 300, largeur_bouton, 5, 0, "Troupes : ", ECRAN_GAUCHE, 0);
  interface->compteur_droite = initBar(1.0/20*W_WINDOW+10, 4.0/5*H_WINDOW + 20, 300, largeur_bouton, 5, 0, "Troupes : ", ECRAN_DROITE, 0);

  interface->fileGauche = init_file(5);
  interface->fileDroite = init_file(5);

  interface->argent = 10;
  interface->epoque_gauche = 0;
  interface->epoque_droite = 0;

  interface->background_gauche = createImage(0, 0, W_WINDOW, H_WINDOW,"Asset/pre.png", ECRAN_GAUCHE, 10);
  interface->background_droite = createImage(0, 0, W_WINDOW, H_WINDOW, "Asset/pre.png",ECRAN_DROITE, 10);

  interface->argent_gauche = initBar(1.0/20*W_WINDOW+40+3*largeur_bouton, 5, 300, largeur_bouton, 1000, interface->argent, "Argent : ", ECRAN_GAUCHE, 0);
  interface->argent_droite = initBar(1.0/20*W_WINDOW+40+3*largeur_bouton, 5, 300, largeur_bouton, 1000, interface->argent, "Argent : ", ECRAN_DROITE, 0);


  interface->bouton_unite1EG = createImage(1.0/20*W_WINDOW+10,5,largeur_bouton,largeur_bouton,"Asset/unite_lourde.png",ECRAN_GAUCHE,10);
  interface->bouton_unite2EG = createImage(1.0/20*W_WINDOW+20+largeur_bouton,5,largeur_bouton,largeur_bouton,"Asset/unite_inter.png",ECRAN_GAUCHE,10);
  interface->bouton_unite3EG = createImage(1.0/20*W_WINDOW+30+2*largeur_bouton,5,largeur_bouton,largeur_bouton,"Asset/unite_legere.png",ECRAN_GAUCHE,10);

  addElementToElement(interface->bouton_unite1EG, blocInterface);
  addElementToElement(interface->bouton_unite2EG, blocInterface);
  addElementToElement(interface->bouton_unite3EG, blocInterface);

  setDataElement(interface->bouton_unite1EG, (void *)bouton_unite);
  setDataElement(interface->bouton_unite2EG, (void *)bouton_unite);
  setDataElement(interface->bouton_unite3EG, (void *)bouton_unite);

  setActionElement(interface->bouton_unite1EG, chrono);
  setActionElement(interface->bouton_unite2EG, chrono);
  setActionElement(interface->bouton_unite3EG, chrono);

  interface->bouton_unite1ED = createImage(1.0/20*W_WINDOW+10,5,largeur_bouton,largeur_bouton,"Asset/unite_lourde.png",ECRAN_DROITE,10);
  interface->bouton_unite2ED = createImage(1.0/20*W_WINDOW+20+largeur_bouton,5,largeur_bouton,largeur_bouton,"Asset/unite_inter.png",ECRAN_DROITE,10);
  interface->bouton_unite3ED = createImage(1.0/20*W_WINDOW+30+2*largeur_bouton,5,largeur_bouton,largeur_bouton,"Asset/unite_legere.png",ECRAN_DROITE,10);

  addElementToElement(interface->bouton_unite1ED, blocInterface);
  addElementToElement(interface->bouton_unite2ED, blocInterface);
  addElementToElement(interface->bouton_unite3ED, blocInterface);

  setDataElement(interface->bouton_unite1ED, (void *)bouton_unite);
  setDataElement(interface->bouton_unite2ED, (void *)bouton_unite);
  setDataElement(interface->bouton_unite3ED, (void *)bouton_unite);

  setActionElement(interface->bouton_unite1ED, chrono);
  setActionElement(interface->bouton_unite2ED, chrono);
  setActionElement(interface->bouton_unite3ED, chrono);

  addClickableElement(interface->bouton_unite1EG,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->bouton_unite1EG,achatUnite1);
  addClickableElement(interface->bouton_unite2EG,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->bouton_unite2EG,achatUnite2);
  addClickableElement(interface->bouton_unite3EG,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->bouton_unite3EG,achatUnite3);

  addClickableElement(interface->bouton_unite1ED,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->bouton_unite1ED,achatUnite1);
  addClickableElement(interface->bouton_unite2ED,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->bouton_unite2ED,achatUnite2);
  addClickableElement(interface->bouton_unite3ED,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->bouton_unite3ED,achatUnite3);

  interface->evolve_gauche = createImage(1.0/20*W_WINDOW+50+3*largeur_bouton+300,5,largeur_bouton,largeur_bouton,"Asset/evolve.png",ECRAN_GAUCHE,10);
  interface->evolve_droite = createImage(1.0/20*W_WINDOW+50+3*largeur_bouton+300,5,largeur_bouton,largeur_bouton,"Asset/evolve.png",ECRAN_DROITE,10);

  addClickableElement(interface->evolve_gauche,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->evolve_gauche,evolve);
  addClickableElement(interface->evolve_droite,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(interface->evolve_droite,evolve);

  addElementToElement(interface->evolve_droite, blocInterface);
  addElementToElement(interface->evolve_gauche, blocInterface);

  interface->baseGauche = createImage(0,2.0/5*H_WINDOW,1.0/10*W_WINDOW,2.0/5*H_WINDOW,"Asset/gauche_caverne.png",ECRAN_GAUCHE,0);
  interface->baseDroite = createImage(9.0/10*W_WINDOW,2.0/5*H_WINDOW,1.0/10*W_WINDOW,2.0/5*H_WINDOW,"Asset/droite_caverne.png",ECRAN_DROITE,0);

  setDataElement(blocInterface, (void *)interface);

  return blocInterface;

}

void achatUnite1(Element * bouton, int i){

  Element * blocInterface;
  lch_t * interface;
  bouton_t * bouton_unite;

  (void)i;

  getDataElement(bouton, (void **)&bouton_unite);

  initIteratorElement(bouton);

  blocInterface = nextIteratorElement(bouton);

  getDataElement(blocInterface, (void **)&interface);

  if(bouton_unite->timer == 0 && !file_pleine(interface->fileGauche)){

    if(interface->argent >= 5){

      interface->argent = interface->argent - 5;
      majBar(interface->argent_gauche,-5,ECRAN_GAUCHE,0);
      majBar(interface->argent_droite,-5,ECRAN_DROITE,0);
      createMob(ECRAN_GAUCHE, 0, blocInterface, interface->fileGauche);
      bouton_unite->timer = bouton_unite->cd;

    }

  }

}

void achatUnite2(Element * bouton, int i){

  Element * blocInterface;
  lch_t * interface;
  bouton_t * bouton_unite;

  (void)i;

  getDataElement(bouton, (void **)&bouton_unite);

  initIteratorElement(bouton);

  blocInterface = nextIteratorElement(bouton);

  getDataElement(blocInterface, (void **)&interface);

  if(bouton_unite->timer == 0 && !file_pleine(interface->fileGauche)){

    if(interface->argent >= 3){

      interface->argent = interface->argent - 3;
      majBar(interface->argent_gauche,-3,ECRAN_GAUCHE,0);
      majBar(interface->argent_droite,-3,ECRAN_DROITE,0);
      createMob(ECRAN_GAUCHE, 1, blocInterface, interface->fileGauche);
      bouton_unite->timer = bouton_unite->cd;

    }

  }

}

void achatUnite3(Element * bouton, int i){

  Element * blocInterface;
  lch_t * interface;
  bouton_t * bouton_unite;

  (void)i;

  getDataElement(bouton, (void **)&bouton_unite);

  initIteratorElement(bouton);

  blocInterface = nextIteratorElement(bouton);

  getDataElement(blocInterface, (void **)&interface);

  if(bouton_unite->timer == 0 && !file_pleine(interface->fileGauche)){

    if(interface->argent >= 2){

      interface->argent = interface->argent - 2;
      majBar(interface->argent_gauche,-2,ECRAN_GAUCHE,0);
      majBar(interface->argent_droite,-2,ECRAN_DROITE,0);
      createMob(ECRAN_GAUCHE, 2, blocInterface, interface->fileGauche);
      bouton_unite->timer = bouton_unite->cd;

    }

  }

}

void chrono(Element * bouton){

  bouton_t * bouton_unite;

  getDataElement(bouton, (void **)&bouton_unite);

  if(bouton_unite->timer > 0){
    bouton_unite->timer --;
  }


}

void evolve(Element * bouton, int i){

    Element * blocInterface;
    lch_t * interface;
    char base[3][50] = {"Asset/gauche_caverne.png","Asset/gauche_chateau.png", "Asset/gauche_tente.png"};
    char background[3][50] = {"Asset/pre.png", "Asset/medieval.png", "Asset/moderne.png"};

    (void)i;

    initIteratorElement(bouton);

    blocInterface = nextIteratorElement(bouton);

    getDataElement(blocInterface, (void **)&interface);

    if(interface->argent >= 100 && interface->epoque_gauche < 2){

      interface->argent = interface->argent - 100;
      majBar(interface->argent_gauche,-100,ECRAN_GAUCHE,0);
      majBar(interface->argent_droite,-100,ECRAN_DROITE,0);
      interface->epoque_gauche = interface->epoque_gauche + 1;
      setImageElement(interface->baseGauche, base[interface->epoque_gauche]);
      setImageElement(interface->background_droite, background[interface->epoque_gauche]);
      setImageElement(interface->background_gauche, background[interface->epoque_gauche]);

    }

}

void evolveEnnemie(Element * lchMob){

    lch_t * interface;
    char base[3][50] = {"Asset/droite_caverne.png","Asset/droite_chateau.png", "Asset/droite_tente.png"};
    char background[3][50] = {"Asset/pre.png", "Asset/medieval.png", "Asset/moderne.png"};

    getDataElement(lchMob, (void **)&interface);

    if(interface->epoque_droite < 2){

      interface->epoque_droite = interface->epoque_droite + 1;
      setImageElement(interface->baseDroite, base[interface->epoque_droite]);
      setImageElement(interface->background_gauche, background[interface->epoque_droite]);
      setImageElement(interface->background_droite, background[interface->epoque_droite]);

    }

}

void IA(Element * lchMob){

  static int timer = 0;
  lch_t * interface;
  mob_t * structMob;

  getDataElement(lchMob, (void **)&interface);

  if(!file_pleine(interface->fileDroite)){
    if(timer == 0){
      if(!file_vide(interface->fileGauche)){

        getDataElement(sommet(interface->fileGauche), (void **)&structMob);

        if(structMob->type == 0){

          createMob(ECRAN_DROITE,2, lchMob, interface->fileDroite);

        }else if(structMob->type == 1){

          createMob(ECRAN_DROITE,1, lchMob, interface->fileDroite);

        }else{

          createMob(ECRAN_DROITE,0, lchMob, interface->fileDroite);

        }

      }else{

        createMob(ECRAN_DROITE,0, lchMob, interface->fileDroite);

      }

      timer = 300;

    }else{

      timer --;

    }
  }

  //printf("%d\n", interface->xp_ennemie);

  if(interface->xp_ennemie >= 100){

    interface->xp_ennemie = 0;

    evolveEnnemie(lchMob);

  }

}
