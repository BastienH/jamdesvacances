#include "game.h"

void startGame(Element * bouton, int i);

int main() {

    int black[4] = {0,0,0,0};
    int white[4] = {255,255,255,0};
    Element * lchMob, * startBouton;
    lch_t * structLch;
    cell_t * cour, * prec;
    start_t * structStart = malloc(sizeof(start_t));
    Element * player;

  	if(!initGame(1300,700,"Age of Evolve",black,0)){

      startBouton = createText(W_WINDOW/4.0 , H_WINDOW/4.0, W_WINDOW/2.0 , H_WINDOW/2.0, 90, "Font/Enchanted.ttf", "START", white, SANDAL2_BLENDED, START, 0);

      addClickableElement(startBouton,rectangleClickable(0.f,0.f,1.f,1.f),0);
      setOnClickElement(startBouton,startGame);

      createImage(0,0,W_WINDOW, H_WINDOW, "Asset/start.png", START, 10);
      player = createImage(3.0/8*W_WINDOW , H_WINDOW/8.0, W_WINDOW/4.0 , 3.0/4*H_WINDOW, "Asset/spritesheet.png", START, 1);

      addAnimationElement(player,0);

      for(int k=0;k<5;k++){

        addSpriteAnimationElement(player,0,0+100*k,0,100,200,8,k);

      }
      setWaySpriteAnimationElement(player,0,1);

      structStart->run = 0;

      setDataElement(startBouton, (void *)structStart);

      initTerrain(W_WINDOW,H_WINDOW);
      lchMob = init_interface();

      getDataElement(lchMob, (void **)&structLch);

      setDataElement(lchMob, (void *)structLch);

      while(!runGame(16) && structLch->pv_droite && structLch->pv_gauche){

        if(structStart->run){

          //initIteratorElement(lchMob);
          cour = structLch->fileGauche->file;

          while(cour){


            marche(cour->mob, lchMob, &(structLch->fileGauche), &(structLch->fileDroite), prec->mob);

            prec = cour;
            cour = cour->suivant;

          }

          cour = structLch->fileDroite->file;

          while(cour){


            marche(cour->mob, lchMob, &(structLch->fileDroite), &(structLch->fileGauche), prec->mob);

            prec = cour;
            cour = cour->suivant;

          }

          IA(lchMob);

        }

      }

      createImage(0,0,W_WINDOW, H_WINDOW, "Asset/end.png", END, 10);

      setDisplayCodeWindow(END);

      if(structLch->pv_gauche <= 0){

        createText(W_WINDOW/4.0 , H_WINDOW/4.0, W_WINDOW/2.0 , H_WINDOW/2.0, 90, "Font/Enchanted.ttf", "Vous avez perdu !", white, SANDAL2_BLENDED, END, 0);

      }else if(structLch->pv_droite <= 0){

        createText(W_WINDOW/4.0 , H_WINDOW/4.0, W_WINDOW/2.0 , H_WINDOW/2.0, 90, "Font/Enchanted.ttf", "Vous avez gagne !", white, SANDAL2_BLENDED, END, 0);

      }

      while(!runGame(16));

      endGame();
      liberer(structLch->fileGauche);
      liberer(structLch->fileDroite);

    }

  return 0;
}


void startGame(Element * bouton, int i){

  start_t * structStart;

  (void)i;

  getDataElement(bouton, (void **)&structStart);

  structStart->run = 1;

  setDisplayCodeWindow(ECRAN_GAUCHE);

  clearDisplayCode(START);

}
