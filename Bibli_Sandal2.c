#include "Bibli_Sandal2.h"

int initGame(int longueur, int largeur, char * nom, int couleur[4], int boolMusic){

  int res = 0;

  if(initAllSANDAL2(IMG_INIT_PNG)){
    puts("Failed to init SANDAL2");
    res = -1;
  }
  else
  {
    if(createWindow(longueur,largeur,nom,0,couleur,0))
    {
      puts("Failed to open the window");
      closeAllSANDAL2();
      res = -2;
    }
    else if(boolMusic)
    {
      SDL_Init(SDL_INIT_AUDIO);
      Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,2048);
    }
  }

  return res;
}

int runGame(int i)
{
  updateWindow();
  displayWindow();

  SDL_Delay(i);
  
  return (PollEvent(NULL));
}

void endGame(void)
{
  closeAllWindow();
  closeAllSANDAL2();
}

void initButton(int x, int y, int taille, char * txt, int colorTxt[4], int colorClique[4], int colorBloc[4], char * source, int option, int param, int display, int plan)
{
  int n = strlen(txt);
  Element * bt = NULL;
  button_t * databt = NULL;

  if(-1 < option && option < 2)
  {
    if(source)
    {
      bt = createText(x,y,11*n*taille,35*taille,90.f,source,txt,colorTxt,SANDAL2_BLENDED,display,plan);
    }
    else
    {
      bt = createText(x,y,11*n*taille,35*taille,90.f,"Font/arial.ttf",txt,colorTxt,SANDAL2_BLENDED,display,plan);
    }
  }
  else if (1 < option && option < 4)
  {
    if(source)
    {
      bt = createButton(x,y,11*n*taille,35*taille,90.f,source,txt,colorTxt,SANDAL2_BLENDED,colorBloc,display,plan);
    }
    else
    {
      bt = createButton(x,y,11*n*taille,35*taille,90.f,"Font/arial.ttf",txt,colorTxt,SANDAL2_BLENDED,colorBloc,display,plan);
    }
  }
  else
  {

  }

  if(bt)
  {
    databt = initDataButton(colorClique,option,param,display,source,txt,colorBloc);
    //showbt(databt);
    bt->data = databt;
    addClickableElement(bt,rectangleClickable(0.f,0.f,1.f,1.f),0);
    setOnClickElement(bt,CliqueBtn);
    setUnClickElement(bt,UnCliqueBtn);
  }
}

void CliqueBtn(Element * this, int i)
{
  (void) i;
  button_t * databt = NULL;

  if(this)
  {
    databt = this->data;
    if(databt)
    {
      if(databt->option == 1)
      {
        if(!createText(this->x,this->y,this->width,this->height,90.f,databt->source,databt->txt,databt->colorClique,SANDAL2_BLENDED,databt->display,BUTTON_ON_p)) puts("Error");
      }
      else if(databt->option == 3)
      {
        if(!createButton(this->x,this->y,this->width,this->height,90.f,databt->source,databt->txt,databt->colorClique,SANDAL2_BLENDED,databt->colorBloc,databt->display,BUTTON_ON_p)) puts("Error");
      }
    }
  }
}

void UnCliqueBtn(Element * this, int i)
{
  (void) i;
  button_t * databt = NULL;
  int option = 0;

  if(this)
  {
    databt = this->data;
    if(databt)
    {
      option = databt->option;
      clearPlanDisplayCode(databt->display,BUTTON_ON_p);
      switch (option)
      {
        case 1:
        setDisplayCodeWindow(databt->param);
        break;
        case 3:
        setDisplayCodeWindow(databt->param);
        break;
        default:
        break;
      }
    }
  }
}
