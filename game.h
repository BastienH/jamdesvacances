#ifndef __GAME__
#define __GAME__

#include "file.h"

typedef struct{
  file_t * fileGauche;
  file_t * fileDroite;
  //interface
  Element * argent_droite;
  Element * argent_gauche;
  Element * bouton_unite1EG;
  Element * bouton_unite2EG;
  Element * bouton_unite3EG;
  Element * bouton_unite1ED;
  Element * bouton_unite2ED;
  Element * bouton_unite3ED;
  Element * evolve_gauche;
  Element * evolve_droite;
  Element * pdv_gauche;
  Element * pdv_droite;
  Element * compteur_droite;
  Element * compteur_gauche;
  //variables
  int argent;
  int epoque_gauche;
  int epoque_droite;
  int pv_gauche;
  int pv_droite;
  int xp_ennemie;
  //terrain
  Element * baseGauche;
  Element * baseDroite;
  Element * background_gauche;
  Element * background_droite;

}lch_t;

typedef struct{
  int cd;
  int timer;
}bouton_t;

void initTerrain(int largeur, int hauteur);
void GaucheVersDroite(Element * fleche, int i);
void DroiteVersGauche(Element * fleche, int i);

void createMob(int ecran, int type, Element * lchMob, file_t * file);
void marche(Element * mob, Element * lchMob, file_t ** fileAlliee, file_t ** fileEnnemie, Element * precedent);

Element * changement_ecran(Element * mob, file_t * f);

void fight(Element * mob, Element * ennemie, Element * lchMob);
void tmort(Element * mob, Element * lchMob);

Element * init_interface();

void achatUnite1(Element * bouton, int i);
void achatUnite2(Element * bouton, int i);
void achatUnite3(Element * bouton, int i);

void chrono(Element * bouton);

void evolve(Element * bouton, int i);
void evolveEnnemie(Element * lchMob);

void IA(Element * lchMob);

#endif
