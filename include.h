#ifndef __INCLUDE__
#define __INCLUDE__

#include "Bibli_Sandal2.h"

#define W_WINDOW 1300
#define H_WINDOW 700
#define START 0
#define ECRAN_GAUCHE 1
#define ECRAN_DROITE 2
#define END 3

typedef struct{
  int type;
  int ecran;
  int vitesse;
  int attack_speed;
  int timer;
  int pv_max;
  int pv;
  int dmg;
  int cout;
  char * sprite;
}mob_t;

typedef struct{
  int run;
}start_t;


#endif
