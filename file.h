#ifndef __FILE_H__
#define __FILE_H__

#include "include.h"

typedef Element * ElemFile;

typedef struct cell{
	ElemFile mob;
	struct cell * suivant;
}cell_t;

typedef struct{
	int taille;
	int NbElem;
	cell_t * file;
}file_t;

file_t * init_file (int n);
void entree (file_t * f, ElemFile v);
ElemFile sortie (file_t * f);
void liberer (file_t * f);
int file_vide (file_t * f);
int file_pleine (file_t * f);
ElemFile sommet (file_t * f);
cell_t * rechercheFile(file_t * f, ElemFile v);

#endif
