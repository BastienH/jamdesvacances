#include "structures.h"

perso_t * initDataPerso(int speed, int gravite, int hauteur_saut)
{
  perso_t * p = malloc(sizeof(perso_t));
  inventaire_t * sac = malloc(sizeof(inventaire_t));

  if( p && sac)
  {
    p->up = 0;
    p->left = 0;
    p->down = 0;
    p->right = 0;
    p->speed = speed;
    p->gravite = gravite;
    p->hauteur_saut = hauteur_saut;
    p->jump = 0;
    p->dt = 0;
    p->action = 0;
    p->sac = sac;
    p->sac->objet = NULL;
  }else{

    free(p);
    free(sac);

  }

  return p;
}

button_t * initDataButton(int colorClique[4], int option, int param, int display, char * source, char * txt, int colorBloc[4])
{
  button_t * databt = malloc(sizeof(button_t));
  int i = 0;

  if(databt)
  {
    databt->option = option;
    databt->param = param;
    databt->display = display;
    databt->source = source;
    databt->txt = txt;
    for (i = 0; i < 4; i ++)
    {
      databt->colorClique[i] = colorClique[i];
      databt->colorBloc[i] = colorBloc[i];
    }
  }

  return databt;
}

void showbt(button_t * databt)
{
  if(databt)
  {
    printf("option :%d\nparam :%d\ndisplay :%d\nsource :%s\ntxt :%s\n", databt->option, databt->param, databt->display, databt->source, databt->txt);
  }
}

platform_t * initDataPlatform(Element * player,int speed, int xdepart, int ydepart, int xarrive, int yarrive){

 platform_t * structPlatform = malloc(sizeof(platform_t));

 if(structPlatform){

   structPlatform->xdepart = xdepart;
   structPlatform->xarrive = xarrive;
   structPlatform->ydepart = ydepart;
   structPlatform->yarrive = yarrive;
   structPlatform->player = player;
   structPlatform->speed = speed;

 }

 return structPlatform;

}
