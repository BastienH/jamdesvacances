# Bibli_Sandal2 (Bibliothèque créé par Bastien Herreros et Théo Boucher respectivement Président et Responsable Communication du club Pixel année 2019/2020)

Cette bibliothèque a été créé dans le but de rendre la Sandal encore plus accessible
afin que tous ses utilisateurs puissent l'appréhender facilement.

Contenu de la bibliothèque :

Les fichiers :
 - "Bibli_Sandal2.c"
 #
 contenant les fonctions suivantes :

    - initGame
    - runGame
    - endGame
    - initButton
    - CliqueBtn
    - UnCliqueBtn

 - "Bibli_Sandal2.h"
 #
 contenant les prototypes et les commentaires associés aux fonctions du fichier "Bibli_Sandal2.c"

- "mouvement.c"
 #
 contenant les fonctions suivantes :

   - initPlayer
   - addAnimation
   - setCollision
   - key_press_nograv
   - key_release_nograv
   - key_press_grav
   - key_release_grav
   - movePlayerGravNoWallJump
   - movePlayerGravWallJump
   - movePlayerNograv

 #

- "mouvement.h"
 #
 contenant les prototypes et les commentaires associés aux fonctions du fichier "mouvement.c"

 - "niveau.c"
  #
  contenant les fonctions suivantes :

    - createPlatform
    - mooving_platform
    - interactive_platform

  #

 - "niveau.h"
  #
  contenant les prototypes et les commentaires associés aux fonctions du fichier "niveau.c"

  - "structures.c"
   #
   contenant les fonctions suivantes :

     - initDataPerso
     - initDataButton
     - initDataPlatform

   #

  - "structures.h"
   #
   contenant les prototypes et les commentaires associés aux fonctions du fichier "structures.c"

   contenant les structures suivantes :

     - inventaire_t
     - perso_t
     - button_t
     - platform_t
     - bar_t


  - "interface.c"
  #
  contenant les fonctions suivantes :

    - initBar
    - majBar

  #

 - "interface.h"
  #
  contenant les prototypes et les commentaires associés aux fonctions du fichier "interface.c"


Les répertoires :
 - Asset
 #
 à remplir avec toutes les assets du jeu
 #
 contenu de base :

    - à compléter
    -
    -
    -

 - Music
 #
 à remplir avec toutes les musiques du jeu
 #
 contenu de base :

    - à compléter
    -
    -
    -

 - Font
 #
 à remplir avec toutes les polices du jeu
 #
 contenu de base :

    - à compléter
    -
    -
    -

Le Makefile :
 - permet de compiler tous les fichier .c du répertoire courant à l'aide de la commande : make
 - possibilité de modifier le nom de l'éxecutable à partir de la ligne 4 : EXE = nomExecutable
 - suppression des fichier .o du répertoire courant avec la commande : make clean
